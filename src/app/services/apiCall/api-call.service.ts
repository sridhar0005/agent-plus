import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiCallService {

  constructor(private http: HttpClient) { }
   apiUrl = 'http://localhost:3000/';

   login(loginData) {
     console.log(loginData);
     return this.http.post(this.apiUrl + 'login', loginData);
   }


   editProfile(editData){
console.log(editData);
return this.http.put(this.apiUrl+'editProfile',editData);
   }

   chartc(data){
console.log(data);
return this.http.get(this.apiUrl+'chartData',data);
   }
}
