import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResouceCenterComponent } from './resouce-center.component';

describe('ResouceCenterComponent', () => {
  let component: ResouceCenterComponent;
  let fixture: ComponentFixture<ResouceCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResouceCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResouceCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
