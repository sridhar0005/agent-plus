import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentContributionComponent } from './agent-contribution.component';

describe('AgentContributionComponent', () => {
  let component: AgentContributionComponent;
  let fixture: ComponentFixture<AgentContributionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentContributionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentContributionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
