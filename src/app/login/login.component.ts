import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiCallService } from '../services/apiCall/api-call.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router,
              private apiCall: ApiCallService) { }

  ngOnInit() {
  }

  login(data) {
    console.log(data);
    this.apiCall.login(data).subscribe((res: any) => {
      console.log(res);

      if (res.success) {
     alert(res.message);
     this.router.navigate (['/dashboard']);

  } else  {
   alert(res.message);
  }
    },
      (err: any) => {
        console.log(err);
        alert(err.message);
      });

  }


}
