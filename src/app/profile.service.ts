import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  url = 'http://localhost:3000/profileEdit';
  constructor(private http: HttpClient) { }

  profileEdit(name,Email, address,address2,city,zip) {
    const obj = {
      name:name,
      Email: Email,
      address:address,
      address2:address2,
      city:city,
      zip: zip
    };
    console.log(obj);
    this.http.post(`${this.url}/add`, obj)
        .subscribe(res => console.log('Done'));
  }
}
