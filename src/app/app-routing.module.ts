import { NgModule} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MainComponent } from './main/main.component';

import { NoticeBoardComponent } from './notice-board/notice-board.component';
import { GreetingsComponent } from './greetings/greetings.component';
import { PlansComponent } from './resourceCenter/plans/plans.component';
import { LearningComponent } from './resourceCenter/learning/learning.component';
import { InfoComponent } from './resourceCenter/info/info.component';

import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { SetTargetsComponent } from './BusinessPlan/set-targets/set-targets.component';
import { EditRequestsComponent } from './BusinessPlan/edit-requests/edit-requests.component';
import { TrackComponent } from './BusinessPlan/track/track.component';
import { BusinessPlanComponent } from './BusinessPlan/business-plan/business-plan.component';

import { ResouceCenterComponent } from './resourceCenter/resouce-center/resouce-center.component';
import { AgentContributionComponent } from './resourceCenter/agent-contribution/agent-contribution.component';
import { AgentComponent } from './agents/Agent/agent.component';
import { AddAgentComponent } from './agents/add-agent/add-agent.component';
import { ApproveAgentsComponent } from './agents/approve-agents/approve-agents.component';
import { ViewAgentsComponent } from './agents/view-agents/view-agents.component';
import { AddfilesComponent } from './addfiles/addfiles.component';
import { ChartComponent } from './chart/chart.component';

import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { SignupComponent } from './signup/signup.component';


const routes: Routes = [
  {
    path: '', component: MainComponent, children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: DashboardComponent,children:[{ path: '', redirectTo: 'chart', pathMatch: 'full' },{

        path:'chart',component:ChartComponent

      }]},
      { path: 'editProfile', component: ProfileComponent },

      {path: 'agent', component: AgentComponent, children: [
        {path:'', redirectTo:'add',pathMatch:'full'},
        {path: 'add', component: AddAgentComponent},
        {path: 'approve' , component: ApproveAgentsComponent},
        {path: 'view' , component: ViewAgentsComponent}
      ]},




      {
        path: 'business-plan', component: BusinessPlanComponent, children: [
          { path: '', redirectTo: 'setTargets', pathMatch: 'full' },
          { path: 'setTargets', component: SetTargetsComponent },
          { path: 'edit', component: EditRequestsComponent },
          { path: 'track', component: TrackComponent },
        ]
      },
      {path: 'resource', component: ResouceCenterComponent, children: [
        {path:'', redirectTo:'plan',pathMatch:'full'},
        {path:'contribution',component:AgentContributionComponent},
        {path: 'info', component: InfoComponent},
        { path: 'plan', component: PlansComponent },
      { path: 'learning', component: LearningComponent },

      ]},






      { path: 'notice', component: NoticeBoardComponent },


      { path: 'greetings', component: GreetingsComponent },

      { path: 'info', component: InfoComponent },
      {path: 'addfile', component: AddfilesComponent},
      {path:'changePassword',component:ChangepasswordComponent}

    ]
  },
  { path: 'login', component: LoginComponent },
  {path:'signup',component:SignupComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
