import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiCallService } from '../services/apiCall/api-call.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProfileService } from '../profile.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  angForm: FormGroup;
  constructor(private router: Router,
    private apiCall: ApiCallService, private fb: FormBuilder, private cd: ProfileService) { this.editForm(); }


  editForm() {
    this.angForm = this.fb.group({
      name: ['', Validators.required],
      Email: ['', Validators.required],
      address: ['', Validators.required],
      address2: ['', Validators.required],
      city: ['', Validators.required],
      zip: ['', Validators.required]
    });
    console.log('profile updated');
  }

  ngOnInit() {

  }
  profileEdit(name, Email, address, address2, city, zip) {
    this.cd.profileEdit(name, Email, address, address2, city, zip);




  }
}
