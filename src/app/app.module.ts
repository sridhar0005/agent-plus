import { BrowserModule } from '@angular/platform-browser';
import { NgModule , NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA, } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import {
  MatSidenavModule, MatIconModule, MatButtonModule,
  MatMenuModule, MatToolbarModule, MatListModule, MatExpansionModule,
  MatCardModule, MatFormFieldModule, MatInputModule}from '@angular/material';

// mdb
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { WavesModule, TableModule, InputsModule} from 'angular-bootstrap-md';
// MDB Angular Free
import { CheckboxModule,  ButtonsModule,  IconsModule, CardsModule } from 'angular-bootstrap-md';
import { DropdownModule } from 'angular-bootstrap-md';


// mdb

import { ChartsModule } from 'ng2-charts';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { NoticeBoardComponent } from './notice-board/notice-board.component';
import { GreetingsComponent } from './greetings/greetings.component';
import { PlansComponent } from './resourceCenter/plans/plans.component';
import { LearningComponent } from './resourceCenter/learning/learning.component';
import { InfoComponent } from './resourceCenter/info/info.component';

import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';



import { AgentContributionComponent } from './resourceCenter/agent-contribution/agent-contribution.component';
import { SetTargetsComponent } from './BusinessPlan/set-targets/set-targets.component';
import {EditRequestsComponent} from './BusinessPlan/edit-requests/edit-requests.component';
import { TrackComponent } from './BusinessPlan/track/track.component';
import { BusinessPlanComponent } from './BusinessPlan/business-plan/business-plan.component';
import { ResouceCenterComponent } from './resourceCenter/resouce-center/resouce-center.component';
import {AgentComponent} from './agents/Agent/agent.component';
import { AddAgentComponent } from './agents/add-agent/add-agent.component';
import { ApproveAgentsComponent } from './agents/approve-agents/approve-agents.component';
import { ViewAgentsComponent } from './agents/view-agents/view-agents.component';
import { AddfilesComponent } from './addfiles/addfiles.component';
import {MatDialogModule} from '@angular/material/dialog';
import { SignupComponent } from './signup/signup.component';
import { ChartComponent } from './chart/chart.component';

import { ChangepasswordComponent } from './changepassword/changepassword.component';

import {ProfileService} from './profile.service';
@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    DashboardComponent,

    NoticeBoardComponent,
    GreetingsComponent,
    PlansComponent,
    LearningComponent,
    InfoComponent,
    EditRequestsComponent,

    LoginComponent,
    ProfileComponent,


    AgentContributionComponent,
    SetTargetsComponent,
    TrackComponent,
    BusinessPlanComponent,
    ResouceCenterComponent,
    AgentComponent,
    AddAgentComponent,
    ApproveAgentsComponent,
    ViewAgentsComponent,
    AddfilesComponent,
    SignupComponent,
    ChartComponent,
    ChangepasswordComponent,




  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule, ReactiveFormsModule,
    MatSidenavModule,
    MatIconModule,
    MatToolbarModule,
    MatListModule,
    MatButtonModule,
    MatMenuModule,
    MatCardModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    HttpClientModule,
    MatDialogModule,
    MDBBootstrapModule,
    ChartsModule,
    WavesModule, TableModule, InputsModule,
    CheckboxModule,  ButtonsModule,  IconsModule, CardsModule, DropdownModule







  ],
  providers: [ProfileService],
  schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA, ],
  bootstrap: [AppComponent],

})
export class AppModule { }
