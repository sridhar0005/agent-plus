const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors')



/************ db connection **************/
mongoose.connect('mongodb://localhost/licV1', {useNewUrlParser: true});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
console.log('mongodb connected to');
});

/******** Declarations *************/
const login = require('./server/routers/loginRouter');
const newAdmin = require('./server/routers/newAdminRouter');
const profile = require('./AgenctPlusServer copy1/routers/editRouter');



/********** middlewares ***********/
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors());

/*************** routes ************/
app.use('/login', login);
app.use('/newAdmin', newAdmin);
app.use('/profileEdit', profile);






app.get('/test', (req, res) => {
    res.send('Server is running');
})

app.listen(3000, () => {
console.log('server is running')
})
