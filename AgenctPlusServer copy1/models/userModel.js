const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    name: {

        type: String,
        required: true
    },
    email: {
      unique:true,
        type: String,
        required: true
    },

    password: {
      type: String,
      required: true,
      maximum:8
  },
    address: {
        type: String,
        required: true
    },
   address2: {

        type: String,
        required: true
    },
   city: {
        type: String,
        required: true
    },
    zip: String,

    target: Number,
    acheivements: Number,
    soldPolicies: Number
},
    {
        timestamps: true
    });

    const User = mongoose.model('User', userSchema);

    module.exports = User;
