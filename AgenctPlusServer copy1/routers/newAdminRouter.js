const express = require('express');
const router = express.Router();
const adminC = require('../controllers/newAdminController');

/****** 
 API: Admin Creation
I/p: 
{
role: Deo
email: string
address: string
mobile: number
} **********/

router.post('/', adminC.create);

module.exports = router;
