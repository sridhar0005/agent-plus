const express = require('express');
const router = express.Router();
const loginC = require('../controllers/loginController');


router.post('/', loginC.login)

module.exports = router;
