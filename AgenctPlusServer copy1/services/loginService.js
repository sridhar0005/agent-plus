
const User = require('../models/userModel');

module.exports.login = async (email, password) => {

    const query = { email: email }
    try {
        const user = await User.findOne(query);
        /******if user exists */
        if (user) {
           // console.log(user);
            if (user.password === password) {
             return true;
            } else {
                throw 'Incorrect Password'
            }
        } else {
            throw "User doesn't exists"

        }
    }
    catch (err) {
        throw err;
    }
}