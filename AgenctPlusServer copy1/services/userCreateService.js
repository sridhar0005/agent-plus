

module.exports.create = async (user) => {
    try {
        const userCreated = await user.save(); // to make synchrnous
        if (userCreated) {
            // console.log(userCreated)
            return userCreated;
        }
    } catch (err) {
        throw err;
    }
};

