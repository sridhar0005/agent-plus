const User = require('../models/userModel');
const userCreate = require('../services/userCreateService');


module.exports.create = async (req, res) => {
    const { name, email,password, address,address2, city,zip } = req.body;
    const user = new User({
        name: name,
       email: email,
       password:password,
       address: address,
        address2: address2,
       city: city,
       zip:zip
    });

    /********service call *********/
    try {
       await userCreate.create(user);
        // console.log(userCreated)
        res.send({ success: true, message: 'Admin Created' });
    } catch (err) {
        console.log('err in admin controller', err);
        res.status(400).send({ success: false, message: err.message })
    }
}

