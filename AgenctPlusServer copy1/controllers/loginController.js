
const  loginS = require('../services/loginService');


module.exports.login = async (req, res) => {
    const { email, password } = req.body;

    try {
       await loginS.login(email, password);
       res.send({success: true, message: 'Logged in!'});
    }
    catch (err) {
        res.send({success: false, message: err});
    }

}