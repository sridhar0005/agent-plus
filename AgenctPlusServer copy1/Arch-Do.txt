App  Architecture

Roles: 
1. Agent
2. Deo

API Documentation

1. Registration --> Create / update
Http Method: POST
I/p: 
role: Deo
email: string
address: string
mobile: number

2. Login
Http Method: POST
I/p: 
Role: Deo
email: string
password: string


3. Dashboard summary 
GET

4. Invite agents -- Send email using server

Http Method: POST
I/p: email: string 

5. Agent Creation : New sign-up form - to be filled by agent
API: /agent 
Http Method: POST
I/p: name: string
     email: string
     password: string
     phoneNumber: string
     address: string
     approved: boolean (default: false)
     target: number
     acheivements: number
     soldPolicies: number
     

6. Get agents
API: /agent
Http Method: GET

7. Approve Agents: 
API: /agent/approve
Http Method: POST
I/p: _id: string,
     approve: boolean(T/F)


